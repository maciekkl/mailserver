﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailSystem.Data
{
    public static class DbInitializer
    {
        public static void Initialize(MailContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
