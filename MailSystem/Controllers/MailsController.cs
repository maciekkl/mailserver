﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MailSystem.Data;
using MailSystem.Models;

namespace MailSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailsController : ControllerBase
    {
        private readonly MailContext _context;

        public MailsController(MailContext context)
        {
            _context = context;
        }

        // GET: api/Mails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Mail>>> GetMails()
        {
            return await _context.Mails.ToListAsync();
        }

        // GET: api/Mails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Mail>> GetMail(int id)
        {
            var mail = await _context.Mails.FindAsync(id);

            if (mail == null)
            {
                return NotFound();
            }

            return mail;
        }

        // PUT: api/Mails/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<ActionResult<Mail>> PutMail(int id, Mail mail)
        {
            if (id != mail.ID)
            {
                return BadRequest();
            }

            _context.Entry(mail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return mail;
        }

        // POST: api/Mails
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Mail>> PostMail(Mail mail)
        {
            _context.Mails.Add(mail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMail", new { id = mail.ID }, mail);
        }

        // DELETE: api/Mails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Mail>> DeleteMail(int id)
        {
            var mail = await _context.Mails.FindAsync(id);
            if (mail == null)
            {
                return NotFound();
            }

            var attachments = await _context.MailAttachments.Where(x => x.MailID == id).ToListAsync();
            foreach (MailAttachment attachment in attachments)
                await DeleteMailAttachment(id, attachment.ID);

            _context.Mails.Remove(mail);
            await _context.SaveChangesAsync();

            return mail;
        }

        private bool MailExists(int id)
        {
            return _context.Mails.Any(e => e.ID == id);
        }

        // POST: api/Mails/SendAllPendingWithCompanyAccount
        [HttpPost("SendAllPendingWithCompanyAccount")]
        public async Task<ActionResult<IEnumerable<Mail>>> SendAllPendingWithCompanyAccount()
        {
            var mails = await _context.Mails.Where(x => x.Status == MailStatus.Pending).ToListAsync();
            SMTPMailSystem mailSystem = new SMTPMailSystem() { MailServer = Startup.MAILSERVER };

            foreach (var mail in mails)
            {
                mail.Attachments = await _context.MailAttachments.Where(x => x.MailID == mail.ID).ToListAsync();
                string originalSender = mail.Sender;
                mail.Sender = Startup.COMPANYMAIL;
                if (!mail.Send(mailSystem))
                {
                    mail.Sender = originalSender;
                    // failure logic                    
                }
                await _context.SaveChangesAsync();
            }

            return mails;
        }

        #region Mail Attachments
        // GET: api/Mails/5/Attachments
        [HttpGet("{mailId}/Attachments")]
        public async Task<ActionResult<IEnumerable<MailAttachment>>> GetMailAttachments(int mailId)
        {
            var mail = await _context.Mails.FindAsync(mailId);
            if (mail == null)
            {
                return NotFound();
            }
            return await _context.MailAttachments.Where(x => x.MailID == mailId).ToListAsync();
        }

        // GET: api/Attachments/5/3
        [HttpGet("{mailId}/Attachments/{attachmentId}")]
        public async Task<ActionResult<MailAttachment>> GetMailAttachment(int mailId, int attachmentId)
        {
            var attachment = await _context.MailAttachments.FindAsync(attachmentId);

            if (attachment == null || attachment.MailID != mailId)
            {
                return NotFound();
            }

            return attachment;
        }

        // PUT: api/Mails/5/Attachments/3
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{mailId}/Attachments/{attachmentId}")]
        public async Task<IActionResult> PutMailAttachment(int mailId, int attachmentId, MailAttachment attachment)
        {
            if (attachmentId != attachment.ID)
            {
                return BadRequest();
            }

            _context.Entry(attachment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MailExists(attachmentId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Mails/5/Attachments
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("{mailId}/Attachments")]
        public async Task<ActionResult<Mail>> PostMailAttachment(int mailId, MailAttachment attachment)
        {
            attachment.MailID = mailId;
            _context.MailAttachments.Add(attachment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMailAttachments", new { mailId = attachment.MailID }, attachment);
        }

        // DELETE: api/Mails/5/Attachments/3
        [HttpDelete("{mailId}/Attachments/{attachmentId}")]
        public async Task<ActionResult<MailAttachment>> DeleteMailAttachment(int mailId, int attachmentId)
        {
            var mail = await _context.Mails.FindAsync(mailId);
            if (mail == null)
            {
                return NotFound();
            }
            var attachment = await _context.MailAttachments.FindAsync(attachmentId);
            if (attachment == null)
            {
                return NotFound();
            }

            _context.MailAttachments.Remove(attachment);
            await _context.SaveChangesAsync();

            return attachment;
        }

        private bool MailAttachmentExists(int id)
        {
            return _context.MailAttachments.Any(e => e.ID == id);
        }

        #endregion
    }
}
