﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MailSystem.Models
{
    public interface IMailSystem
    {
        public bool SendMail(Mail mail);
    }
    public class SMTPMailSystem : IMailSystem
    {
        public string MailServer { get; set; }
        public bool SendMail(Mail mail)
        {
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(mail.Sender);
            foreach (string s in mail.Recipients)
                message.To.Add(s);
            message.Subject = mail.Subject;
            message.Body = mail.Body;
            foreach (var a in mail.Attachments)
                message.Attachments.Add(Attachment.CreateAttachmentFromString(a.Body, a.Name));

            SmtpClient client = new SmtpClient(MailServer);
            client.UseDefaultCredentials = true;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                // handle exception
                return false;
            }
            return true;
        }
    }
}
