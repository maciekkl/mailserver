﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MailSystem.Models
{
    public enum MailPriority
    {
        Low,
        Normal,
        High
    }

    public enum MailStatus
    {
        Pending,
        Sent
    }
    public class Mail
    {
        public int ID { get; set;  }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Sender { get; set; }
        public MailPriority Priority { get; set; }
        public MailStatus Status { get; private set; }

        public string[] Recipients { get; set; }
        public ICollection<MailAttachment> Attachments;

        public bool Send(IMailSystem mailSystem)
        {
            if (Startup.ACTUALLYSEND)
            {
                if (mailSystem.SendMail(this))
                {
                    Status = MailStatus.Sent;
                    return true;
                }
                return false;
            }
            else
            {
                Status = MailStatus.Sent;
                return true;
            }

        }
    }

}
