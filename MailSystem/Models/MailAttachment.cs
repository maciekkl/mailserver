﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailSystem.Models
{
    public class MailAttachment
    {
        public int ID { get; set; }
        public int MailID { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        //public Mail Mail { get; set; }
    }
}
